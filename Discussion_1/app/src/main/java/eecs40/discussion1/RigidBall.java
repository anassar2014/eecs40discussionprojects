package eecs40.discussion1;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;

import java.util.Random;

/**
 * Created by Ahmed on 3/24/2015.
 */
public class RigidBall {
    // Class properties
    private static final int RADIUS       = 100;
    private static final int MIN_VELOCITY = 20;
    private static final int MAX_VELOCITY = 50;
    private static final int COLOR        = Color.YELLOW;

    // Object properties
    private final BubbleWorldView  bwv;
    private final int              mR;
    private final int              mColor;
    private       int              mVelX;
    private       int              mVelY;
    private       int              mX;
    private       int              mY;

    public RigidBall( BubbleWorldView bwv ) {
        Random rng  = new Random();
        this.bwv    = bwv;
        this.mColor = COLOR;
        this.mR     = RADIUS;
        this.mX     = 0;
        this.mY     = 0;
        this.mVelX  = rng.nextInt(MAX_VELOCITY - MIN_VELOCITY + 1) + MIN_VELOCITY;
        this.mVelY  = rng.nextInt(MAX_VELOCITY - MIN_VELOCITY + 1) + MIN_VELOCITY;
    }

    public void draw( Canvas c ) {
        Paint paint = new Paint();
        paint.setColor( mColor );
        c.drawCircle( this.getX(), this.getY(), this.getR(), paint );
        this.stepCoordinates();
    }

    public int getX() { return mX; }
    public int getY() { return mY; }
    public int getR() { return mR; }

    // The laws of physics: Restricted frictionless motion :)
    public void stepCoordinates() {
        int maxX = bwv.getWidth();
        int maxY = bwv.getHeight();

        mX += mVelX;
        mY += mVelY;

        if ( mX > ( maxX - mR) ) {
            mVelX = -mVelX;
            mX = maxX - mR;
        } else if ( mX < mR) {
            mVelX = -mVelX;
            mX = mR;
        }
        if ( mY > ( maxY - mR) ) {
            mVelY = -mVelY;
            mY = maxY - mR;
        } else if ( mY < mR) {
            mVelY = -mVelY;
            mY = mR;
        }
    }
}
