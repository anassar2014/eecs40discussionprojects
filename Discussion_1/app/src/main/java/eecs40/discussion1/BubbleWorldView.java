package eecs40.discussion1;

/**
 * Created by Ahmed on 3/24/2015.
 */

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.content.Context;




public class BubbleWorldView extends SurfaceView implements SurfaceHolder.Callback {

    private RigidBall          ball; // The primordial object
    private BubbleWorldThread  bst;

    public BubbleWorldView(Context context) {
        super(context);
        // Notify the SurfaceHolder that you'd like to receive SurfaceHolder callbacks.
        getHolder().addCallback(this);
        ball = new RigidBall( this );
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        renderGame(canvas);
    }

    protected void renderGame(Canvas c) {
        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setColor(Color.WHITE);
        paint.setAntiAlias(true);
        c.drawPaint(paint); // Fill background

        ball.draw( c );
    }


    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        bst = new BubbleWorldThread(this);
        bst.start();
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // TODO
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        // The cleanest way to stop a thread is by interrupting it.
        // The thread must regularly check its interrupt flag.
        bst.interrupt();
    }
}
